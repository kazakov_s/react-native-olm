# rn-olm

Olm for react native

## Installation

```sh
npm install rn-olm
```
```sh
yarn add rn-olm
```

## Usage

```js
import Olm from "rn-olm";

// ...

const result = await Olm.multiply(3, 7);
```

## Building Locally 

### 1. Install dependencies
```
yarn
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
