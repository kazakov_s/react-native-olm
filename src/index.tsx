import { NativeModules } from 'react-native';

const { ReactNativeOlm } = NativeModules;

const log = (..._args: any[]) => {
  console.log(..._args);
};

log(ReactNativeOlm);

const Olm = {
  init: async () => {
    Olm.PRIVATE_KEY_LENGTH = await ReactNativeOlm.initializeOlm();
  },
  get_library_version: ReactNativeOlm.getOlmLibVersion,
  PRIVATE_KEY_LENGTH: 0,
  accountIdx: 0,
  sessionIdx: 0,
  inboundGroupSessionIdx: 0,
  outboundGroupSessionIdx: 0,
  pkSigningIdx: 0,
  pkEncryptionIdx: 0,
  pkDecryptionIdx: 0,
  sasIdx: 0,
  utilityIdx: 0,
  SAS: class SAS {
    id: number;
    constructor() {
      log('OlmSAS');
      this.id = Olm.sasIdx;
      Olm.sasIdx += 1;
      ReactNativeOlm.createSAS(this.id);
    }
    free() {
      log('SAS free');
      const result = ReactNativeOlm.freeSAS(this.id);
      log('SAS free FINISHED');
      return result;
    }
    get_pubkey() {
      log('SAS get_pubkey');
      const result = ReactNativeOlm.getSASPubKey(this.id);
      log('SAS get_pubkey FINISHED');
      return result;
    }
    set_their_key(their_key: string) {
      log('SAS set_their_key');
      const result = ReactNativeOlm.setTheirSASKey(this.id, their_key);
      log('SAS set_their_key FINISHED');
      return result;
    }
    generate_bytes(info: string, length: number) {
      log('SAS generate_bytes');
      const result = new Uint8Array(
        ReactNativeOlm.generateSASBytes(this.id, info, length)
      );
      log('SAS generate_bytes FINISHED');
      return result;
    }
    calculate_mac(input: string, info: string) {
      log('SAS calculate_mac');
      const result = ReactNativeOlm.calculateSASMac(this.id, input, info);
      log('SAS calculate_mac FINISHED');
      return result;
    }
    calculate_mac_long_kdf(input: string, info: string) {
      log('SAS calculate_mac_long_kdf');
      const result = ReactNativeOlm.calculateSASMacLongKdf(this.id, input, info);
      log('SAS calculate_mac_long_kdf FINISHED');
      return result;
    }
  },
  Session: class Session {
    id: number;
    constructor() {
      log('OlmSession');
      this.id = Olm.sessionIdx;
      Olm.sessionIdx += 1;
      ReactNativeOlm.initializeSession(this.id);
    }
    getId() {
      return this.id;
    }
    free() {
      log('Session: free');
      const result = ReactNativeOlm.freeSession(this.id);
      log('Session: free finished');
      return result;
    }
    pickle(key: string | Uint8Array = 'DEFAULT_KEY') {
      log('Session: pickle');
      const result = ReactNativeOlm.pickleSession(this.id, key);
      log('Session: pickle finished');
      return result;
    }
    unpickle(key: string | Uint8Array = 'DEFAULT_KEY', pickle: string) {
      log('Session: unpickle');
      const result = ReactNativeOlm.unpickleSession(this.id, key, pickle);
      log('Session: unpickle FINISHED');
      return result;
    }
    create_outbound(
      account: any,
      their_identity_key: string,
      their_one_time_key: string
    ) {
      log('Session: create_outbound');
      const result = ReactNativeOlm.createOutboundSession(
        this.id,
        account.getId(),
        their_identity_key,
        their_one_time_key
      );
      log('Session: create_outbound FINISHED');
      return result;
    }
    create_inbound(account: any, one_time_key_message: string) {
      log('Session: create_inbound');
      const result = ReactNativeOlm.createInboundSession(
        this.id,
        account.getId(),
        one_time_key_message
      );
      log('Session: create_inbound FINISHED');
      return result;
    }
    create_inbound_from(
      account: any,
      identity_key: string,
      one_time_key_message: string
    ) {
      log('Session: create_inbound_from');
      const result = ReactNativeOlm.createInboundSessionFrom(
        this.id,
        account.getId(),
        identity_key,
        one_time_key_message
      );
      log('Session: create_inbound_from FINISHED');
      return result;
    }
    session_id() {
      log('Session: session_id');
      const result = ReactNativeOlm.getSessionId(this.id);
      log('Session: session_id FINISHED');
      return result;
    }
    has_received_message() {
      log('Session: has_received_message');
      const result = ReactNativeOlm.sessionHasReceivedMessage(this.id);
      log('Session: has_received_message FINISHED');
      return result;
    }
    matches_inbound(one_time_key_message: string) {
      log('Session: matches_inbound');
      const result = ReactNativeOlm.sessionMatchesInbound(this.id, one_time_key_message);
      log('Session: matches_inbound FINISHED');
      return typeof result === 'string' ? result === 'true' : result;
    }
    matches_inbound_from(identity_key: string, one_time_key_message: string) {
      log('Session: matches_inbound_from');
      const result = ReactNativeOlm.sessionMatchesInboundFrom(
        this.id,
        identity_key,
        one_time_key_message
      );
      log('Session: matches_inbound_from FINISHED');
      return typeof result === 'string' ? result === 'true' : result;
    }
    encrypt(plaintext: string) {
      log('Session: encrypt');
      const result = ReactNativeOlm.encryptSession(this.id, plaintext);
      log('Session: encrypt FINISHED');
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result;
    }
    decrypt(message_type: number, message: string) {
      log('Session: decrypt');
      const result = ReactNativeOlm.decryptSession(this.id, message_type, message);
      log('Session: decrypt FINISHED');
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result.result;
    }
    describe() {
      log('Session: describe');
      const result = ReactNativeOlm.describeSession(this.id);
      log('Session: describe FINISHED');
      return result;
    }
  },
  Utility: class Utility {
    id: number;
    constructor() {
      log('Utility');
      this.id = Olm.utilityIdx;
      Olm.utilityIdx += 1;
      ReactNativeOlm.createUtility(this.id);
    }
    free() {
      log('Utility: free');
      const result = ReactNativeOlm.freeUtility(this.id);
      log('Utility: free FINISHED');
      return result;
    }
    sha256(input: string | Uint8Array) {
      log('Utility: sha256');
      const result = ReactNativeOlm.sha256(this.id, input);
      log('Utility: sha256 FINISHED');
      return result;
    }
    ed25519_verify(
      key: string,
      message: string | Uint8Array,
      signature: string
    ) {
      log('Utility: ed25519_verify');
      const result = ReactNativeOlm.ed25519_verify(this.id, key, message, signature);
      log('Utility: ed25519_verify FINISHED');
      return typeof result === 'string' ? result === 'true' : result;
    }
  },
  PkSigning: class PkSigning {
    id: number;
    constructor() {
      log('PkSigning');
      this.id = Olm.pkSigningIdx;
      Olm.pkSigningIdx += 1;
      ReactNativeOlm.createPkSigning(this.id);
    }
    free() {
      log('PkSigning: free');
      const result = ReactNativeOlm.freePkSigning(this.id);
      log('PkSigning: free FINISHED');
      return result;
    }
    init_with_seed(seed: Uint8Array) {
      log('PkSigning: init_with_seed');
      const result = ReactNativeOlm.initializePkSigningWithSeed(this.id, Array.from(seed));
      log('PkSigning: init_with_seed FINISHED');
      return result;
    }
    generate_seed() {
      log('PkSigning: generate_seed');
      const result = new Uint8Array(ReactNativeOlm.generatePkSigningSeed(this.id));
      log('PkSigning: generate_seed FINISHED');
      return result;
    }
    sign(message: string) {
      log('PkSigning: sign');
      const result = ReactNativeOlm.signPkSigning(this.id, message);
      log('PkSigning: sign FINISHED');
      return result;
    }
  },
  PkEncryption: class PkEncryption {
    id: number;
    constructor() {
      log('OlmPkEncryption');
      this.id = Olm.pkEncryptionIdx;
      Olm.pkEncryptionIdx += 1;
      ReactNativeOlm.createPkEncryption(this.id);
    }
    free() {
      log('PkEncryption: free');
      const result = ReactNativeOlm.freePkEncryption(this.id);
      log('PkEncryption: free FINISHED');
      return result;
    }
    set_recipient_key(key: string) {
      log('PkEncryption: set_recipient_key');
      const result = ReactNativeOlm.setPkEncryptionRecipientKey(this.id, key);
      log('PkEncryption: set_recipient_key FINISHED');
      return result;
    }
    encrypt(plaintext: string) {
      log('PkEncryption: encrypt');
      const result = ReactNativeOlm.encryptPkEncryption(this.id, plaintext);
      log('PkEncryption: encrypt FINISHED');
      return result;
    }
  },
  PkDecryption: class PkDecryption {
    id: number;
    constructor() {
      log('OlmPkDecryption');
      this.id = Olm.pkDecryptionIdx;
      Olm.pkDecryptionIdx += 1;
      ReactNativeOlm.createPkDecryption(this.id);
    }
    free() {
      log('PkDecryption: free');
      const result = ReactNativeOlm.freePkDecryption(this.id);
      log('PkDecryption: free FINISHED');
      return result;
    }
    init_with_private_key(key: Uint8Array) {
      log('PkDecryption: init_with_private_key');
      const result = ReactNativeOlm.initializePkDecryptionWithPrivateKey(
        this.id,
        Array.from(key)
      );
      log('PkDecryption: init_with_private_key FINISHED');
      return result;
    }
    generate_key() {
      log('PkDecryption: generate_key');
      const result = ReactNativeOlm.generatePkDecryptionKey(this.id);
      log('PkDecryption: generate_key FINISHED');
      return result;
    }
    get_private_key() {
      log('PkDecryption: get_private_key');
      const result = ReactNativeOlm.getPkDecryptionPrivateKey(this.id);
      log('PkDecryption: get_private_key FINISHED');
      return result;
    }
    pickle(key: string | Uint8Array = 'DEFAULT_KEY') {
      log('PkDecryption: pickle');
      const result = ReactNativeOlm.picklePkDecryption(this.id, key);
      log('PkDecryption: pickle FINISHED');
      return result;
    }
    unpickle(key: string | Uint8Array = 'DEFAULT_KEY', pickle: string) {
      log('PkDecryption: unpickle');
      const result = ReactNativeOlm.unpicklePkDecryption(this.id, key, pickle);
      log('PkDecryption: unpickle FINISHED');
      return result;
    }
    decrypt(ephemeral_key: string, mac: string, ciphertext: string) {
      log('PkDecryption: decrypt ================>');
      const result = ReactNativeOlm.decryptPkDecryption(
        this.id,
        ephemeral_key,
        mac,
        ciphertext
      );
      log('PkDecryption: decrypt FINISHED');
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result.result;
    }
  },
  OutboundGroupSession: class OutboundGroupSession {
    id: number;
    constructor() {
      // return OlmOutboundGroupSession;
      log('OlmOutboundGroupSession');
      this.id = Olm.outboundGroupSessionIdx;
      Olm.outboundGroupSessionIdx += 1;
      ReactNativeOlm.initializeOutboundGroupSession(this.id);
    }
    getId() {
      return this.id;
    }
    free() {
      log('OutboundGroupSession: free');
      const result = ReactNativeOlm.freeOutboundGroupSession(this.id);
      log('OutboundGroupSession: free FINISHED');
      return result;
    }
    pickle(key: string | Uint8Array = 'DEFAULT_KEY') {
      log('OutboundGroupSession: pickle');
      const result = ReactNativeOlm.pickleOutboundGroupSession(this.id, key);
      log('OutboundGroupSession: pickle FINISHED');
      return result;
    }
    unpickle(key: string | Uint8Array = 'DEFAULT_KEY', pickle: string) {
      log('OutboundGroupSession: unpickle');
      const result = ReactNativeOlm.unpickleOutboundGroupSession(
        this.id,
        key,
        pickle
      );
      log('OutboundGroupSession: unpickle FINISHED');
      return result;
    }
    create() {
      log('OutboundGroupSession: create');
      const result = ReactNativeOlm.createOutboundGroupSession(this.id);
      log('OutboundGroupSession: create FINISHED');
      return result;
    }
    encrypt(plaintext: string) {
      log('OutboundGroupSession: encrypt');
      const result = ReactNativeOlm.encryptOutboundGroupSession(this.id, plaintext);
      log('OutboundGroupSession: encrypt FINISHED');
      return result;
    }
    session_id() {
      log('OutboundGroupSession: session_id');
      const result = ReactNativeOlm.getOutboundGroupSessionSessionId(this.id);
      log('OutboundGroupSession: session_id FINISHED');
      return result;
    }
    session_key() {
      log('OutboundGroupSession: session_key');
      const result = ReactNativeOlm.getOutboundGroupSessionSessionKey(this.id);
      log('OutboundGroupSession: session_key FINISHED');
      return result;
    }
    message_index() {
      log('OutboundGroupSession: message_index');
      const result = ReactNativeOlm.getOutboundGroupSessionMessageIndex(this.id);
      log('OutboundGroupSession: message_index FINISHED');
      return result;
    }
  },
  InboundGroupSession: class InboundGroupSession {
    id: number;
    constructor() {
      // return OlmInboundGroupSession;
      log('OlmInboundGroupSession');
      this.id = Olm.inboundGroupSessionIdx;
      Olm.inboundGroupSessionIdx += 1;
      ReactNativeOlm.initializeInboundGroupSession(this.id);
    }
    getId() {
      return this.id;
    }
    free() {
      log('InboundGroupSession: free');
      const result = ReactNativeOlm.freeInboundGroupSession(this.id);
      log('InboundGroupSession: free FINISHED');
      return result;
    }
    pickle(key: string | Uint8Array = 'DEFAULT_KEY') {
      log('InboundGroupSession: pickle');
      const result = ReactNativeOlm.pickleInboundGroupSession(this.id, key);
      log('InboundGroupSession: pickle FINISHED');
      return result;
    }
    unpickle(key: string | Uint8Array = 'DEFAULT_KEY', pickle: string) {
      log('InboundGroupSession: unpickle');
      const result = ReactNativeOlm.unpickleInboundGroupSession(
        this.id,
        key,
        pickle
      );
      log('InboundGroupSession: unpickle FINISHED');
      return result;
    }
    create(session_key: string) {
      log('InboundGroupSession: create inboundGroupSession');
      const result = ReactNativeOlm.createInboundGroupSession(this.id, session_key);
      log('InboundGroupSession: create inboundGroupSession FINISHED');
      return result;
    }
    import_session(session_key: string) {
      log('InboundGroupSession: import_session');
      const result = ReactNativeOlm.importInboundGroupSession(this.id, session_key);
      log('InboundGroupSession: import_session FINISHED');
      if (result?.error) {
        throw new Error(result?.error);
      }
    }
    decrypt(message: string) {
      log('InboundGroupSession: decrypt inboundGroupSession');
      const result = ReactNativeOlm.decryptInboundGroupSession(this.id, message);
      log('InboundGroupSession: decrypt inboundGroupSession FINISHED');
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result;
    }
    session_id() {
      log('InboundGroupSession: session_id inboundGroupSession');
      const result = ReactNativeOlm.getInboundGroupSessionSessionId(this.id);
      log('InboundGroupSession: session_id inboundGroupSession FINISHED');
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result.result;
    }
    first_known_index() {
      log('InboundGroupSession: inboundGroupSession first_known_index');
      const result = ReactNativeOlm.getInboundGroupSessionFirstKnownIndex(this.id);
      log(
        'InboundGroupSession: inboundGroupSession first_known_index FINISHED'
      );
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result.result;
    }
    export_session(message_index: number) {
      log('InboundGroupSession: export_session');
      const result = ReactNativeOlm.exportInboundGroupSession(this.id, message_index);
      log('InboundGroupSession: export_session FINISHED');
      if (result?.error) {
        throw new Error(result?.errorMessage);
      }
      return result.result;
    }
  },
  Account: class Account {
    id: number;
    constructor() {
      log('OlmAccount');
      this.id = Olm.accountIdx;
      Olm.accountIdx += 1;
      ReactNativeOlm.initializeAccount(this.id);
    }
    getId() {
      return this.id;
    }
    free() {
      log('Account: free');
      ReactNativeOlm.freeAccount(this.id);
      log('Account: free FINISHED');
    }
    create() {
      log('Account: create', this.id);
      ReactNativeOlm.createAccount(this.id);
      log('Account: create FINISHED');
    }
    identity_keys() {
      log('Account: identity_keys');
      const result = ReactNativeOlm.identity_keys(this.id);
      log('Account: identity_keys FINISHED');
      return result;
    }
    sign(message: string | Uint8Array) {
      log('Account: sign');
      const result = ReactNativeOlm.signAccount(this.id, message);
      log('Account: sign FINISHED');
      return result;
    }
    one_time_keys() {
      log('Account: one_time_keys');
      const result = ReactNativeOlm.one_time_keys(this.id);
      log('Account: one_time_keys FINISHED');
      return result;
    }
    mark_keys_as_published() {
      log('Account: mark_keys_as_published');
      const result = ReactNativeOlm.mark_keys_as_published(this.id);
      log('Account: mark_keys_as_published FINISHED');
      return result;
    }
    max_number_of_one_time_keys() {
      log('Account: max_number_of_one_time_keys');
      const result = ReactNativeOlm.max_number_of_one_time_keys(this.id);
      log('Account: max_number_of_one_time_keys FINISHED');
      return result;
    }
    generate_one_time_keys(number_of_keys: number) {
      log('Account: generate_one_time_keys');
      const result = ReactNativeOlm.generate_one_time_keys(this.id, number_of_keys);
      log('Account: generate_one_time_keys FINISHED');
      return result;
    }
    remove_one_time_keys(session: any) {
      log('Account: remove_one_time_keys');
      const result = ReactNativeOlm.remove_one_time_keys(this.id, session.getId());
      log('Account: remove_one_time_keys');
      return result;
    }
    generate_fallback_key() {
      log('Account: generate_fallback_key');
      // FIXME I couldn't find this function in the iOS OLMKit
      const result = ReactNativeOlm.generate_fallback_key(this.id);
      log('Account: generate_fallback_key FINISHED');
      return result;
    }
    fallback_key() {
      log('Account: fallback_key');
      // FIXME I couldn't find this function in the iOS OLMKit
      const result = ReactNativeOlm.fallback_key(this.id);
      log('Account: fallback_key FINISHED');
      return result;
    }
    pickle(_key: string | Uint8Array = 'DEFAULT_KEY') {
      log('Account: pickle');
      const result = ReactNativeOlm.pickleAccount(this.id, _key);
      log('Account: pickle FINISHED');
      return result;
    }
    unpickle(
      _key: string | Uint8Array = 'DEFAULT_KEY',
      pickle: string = 'PICKLE'
    ) {
      log('Account: unpickle');
      const result = ReactNativeOlm.unpickleAccount(this.id, _key, pickle);
      log('Account: unpickle FINISHED');
      return result;
    }
  },
};

export default Olm;
