#import "Olm.h"

@implementation Olm

RCT_EXPORT_MODULE(ReactNativeOlm)

+ (BOOL)requiresMainQueueSetup
{
   return NO;
}

- (id)init
{
    if ((self = [super init])) {
        _accounts = [[NSMutableDictionary alloc] init];
        _olmInboundGroupSessions = [[NSMutableDictionary alloc] init];
        _olmOutboundGroupSessions = [[NSMutableDictionary alloc] init];
        _olmSessions = [[NSMutableDictionary alloc] init];
        _olmPkSignings = [[NSMutableDictionary alloc] init];
        _olmPkEncryptions = [[NSMutableDictionary alloc] init];
        _olmPkDecryptions = [[NSMutableDictionary alloc] init];
        _olmSASs = [[NSMutableDictionary alloc] init];
        _utilities = [[NSMutableDictionary alloc] init];
    }

    return self;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getOlmLibVersion)
{
    return [OLMKit versionString];
}

RCT_EXPORT_METHOD(initializeOlm:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
    NSLog(@"Init Olm");
    _olmKit = [[OLMKit alloc] init].init;
    NSUInteger privateKeyLength = [OLMPkDecryption privateKeyLength];
    resolve(@((long) privateKeyLength));
}

// =============================================>
// Account
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(initializeAccount: (NSUInteger*)accountId)
{
    NSLog(@"Init Account");
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createAccount: (NSUInteger*)accountId)
{
    NSLog(@"Create Account");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [[OLMAccount alloc] init].initNewAccount;
    [_accounts setObject:account forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freeAccount: (NSUInteger*)accountId)
{
    NSLog(@"Free Account");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    [_accounts removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(identity_keys:(NSUInteger*)accountId)
{
    NSLog(@"Identity Keys");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    NSDictionary *result = account.identityKeys;
    NSError *error;
    NSString *testString = [NSString stringWithFormat:@"my dictionary is %@", result];
    NSLog(@"KEYS: %@", testString);
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result options:0 error:&error];
    if (!jsonData) {
        return nil;
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(signAccount:(NSUInteger*)accountId
                                       msg:(NSString*)msg)
{
    NSLog(@"Sign Account");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    NSData* msgData = [msg dataUsingEncoding:NSUTF8StringEncoding];
    NSString *result = [account signMessage:msgData];
    return result;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(one_time_keys:(NSUInteger*)accountId)
{
    NSLog(@"One Time Keys");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    NSDictionary *result = [account oneTimeKeys];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result options:0 error:&error];
    if (!jsonData) {
        return nil;
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(mark_keys_as_published:(NSUInteger*)accountId)
{
    NSLog(@"Mark Keys As Published");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    [account markOneTimeKeysAsPublished];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(max_number_of_one_time_keys:(NSUInteger*)accountId)
{
    NSLog(@"Mark Keys As Published");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    NSNumber *maxKeys = @([account maxOneTimeKeys]);
    return maxKeys;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(generate_one_time_keys:(NSUInteger*)accountId
                                       number_of_keys:(NSUInteger)number_of_keys)
{
    NSLog(@"Generate one time keys");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    [account generateOneTimeKeys:number_of_keys];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(generate_fallback_key:(NSUInteger*)accountId)
{
    NSLog(@"Generate Fallback Key: not implemented");
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(fallback_key:(NSUInteger*)accountId)
{
    NSLog(@"Get Fallback Key: not implemented");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    //NSDictionary *result = [account fallbackKey];
    NSDictionary *result = [[NSDictionary alloc] init];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:result options:0 error:&error];
    if (!jsonData) {
        return nil;
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(pickleAccount:(NSUInteger*)accountId
                                       key:(NSString*)key)
{
    NSLog(@"Pickle Account");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        return nil;
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [account serializeDataWithKey:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(unpickleAccount:(NSUInteger*)accountId
                                       key:(NSString*)key
                                       pickle:(NSString*)pickle)
{
    NSLog(@"Unpickle Account");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id];
    if (account == nil) {
        account = [[OLMAccount alloc] init];
        [_accounts setObject:account forKey:id];
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [account initWithSerializedData:pickle key:keyData error:&error];
}

// =============================================>
// Inbound Group Session
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(initializeInboundGroupSession:(NSUInteger*)sessionId)
{
    NSLog(@"Init Inbound Group Session");
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createInboundGroupSession:(NSUInteger*)sessionId
                                       session_key:(NSString*)session_key)
{
    NSLog(@"Create Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [[OLMInboundGroupSession alloc] init];
    @try {
        NSError *error;
        inboundGroupSession = [inboundGroupSession initInboundGroupSessionWithSessionKey:session_key error:&error];
        if (error) {
            NSLog(@"olmInboundGroupSession create() failed: %@", [error localizedDescription]);
        } else {
            [_olmInboundGroupSessions setObject:inboundGroupSession forKey:id];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"olmInboundGroupSession create() failed: %@", exception.reason);
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freeInboundGroupSession:(NSUInteger *)sessionId)
{
    NSLog(@"Free Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    if (inboundGroupSession == nil) {
        return nil;
    }
    [_olmInboundGroupSessions removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(pickleInboundGroupSession:(NSUInteger *)sessionId
                                       key:(NSString*)key)
{
    NSLog(@"Pickle Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    if (inboundGroupSession == nil) {
        return nil;
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [inboundGroupSession serializeDataWithKey:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(unpickleInboundGroupSession:(NSUInteger*)sessionId
                                       key:(NSString*)key
                                       pickle:(NSString*)pickle)
{
    NSLog(@"Unpickle Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    if (inboundGroupSession == nil) {
        inboundGroupSession = [[OLMInboundGroupSession alloc] init];
    }
    @try {
        NSError *error;
        NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
        OLMInboundGroupSession *unpickledInboundGroupSession = [inboundGroupSession initWithSerializedData:pickle key:keyData error:&error];
        if (error) {
            NSLog(@"olmInboundGroupSession unpickle() failed: %@", [error localizedDescription]);
        } else {
            [_olmInboundGroupSessions setObject:unpickledInboundGroupSession forKey:id];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"olmInboundGroupSession unpickle() failed: %@", exception.reason);
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(importInboundGroupSession:(NSUInteger*)sessionId
                                       session_key:(NSString*)session_key)
{
    NSLog(@"Import Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [[OLMInboundGroupSession alloc] init];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    @try {
        NSError *error;
        inboundGroupSession = [inboundGroupSession initInboundGroupSessionWithImportedSession:session_key error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [_olmInboundGroupSessions setObject:inboundGroupSession forKey:id];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    
    return map;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(decryptInboundGroupSession:(NSUInteger*)sessionId
                                       message:(NSString*)message)
{
    NSLog(@"Decrypt Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (inboundGroupSession == nil) {
        return map;
    }
    @try {
        NSError *error;
        NSUInteger messageIndex;
        NSString *decryptedMessage = [inboundGroupSession decryptMessage:message messageIndex:&messageIndex error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [map setObject:decryptedMessage forKey:@"plaintext"];
            [map setObject:@((long)messageIndex) forKey:@"message_index"];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    
    return map;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getInboundGroupSessionSessionId:(NSUInteger*)sessionId)
{
    NSLog(@"Get Inbound Group Session Session ID");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (inboundGroupSession == nil) {
        return map;
    }
    NSString *sessionIdentifier;
    @try {
        sessionIdentifier = [inboundGroupSession sessionIdentifier];
        [map setObject:sessionIdentifier forKey:@"result"];
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getInboundGroupSessionFirstKnownIndex:(NSUInteger*)sessionId)
{
    NSLog(@"Get Inbound Group Session First Known Index");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (inboundGroupSession == nil) {
        return map;
    }
    @try {
        NSNumber *index = @([inboundGroupSession firstKnownIndex]);
        [map setObject:index forKey:@"result"];
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(exportInboundGroupSession:(NSUInteger*)sessionId
                                       message_index:(NSUInteger*)message_index)
{
    NSLog(@"Export Inbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMInboundGroupSession *inboundGroupSession = [_olmInboundGroupSessions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (inboundGroupSession == nil) {
        return map;
    }
    @try {
        NSError *error;
        NSUInteger messageIndex = (NSUInteger) message_index;
        NSString *exportedInboundGroupSession = [inboundGroupSession exportSessionAtMessageIndex:messageIndex error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [map setObject:exportedInboundGroupSession forKey:@"result"];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

// =============================================>
// Utility
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createUtility:(NSUInteger*)utilityId)
{
    NSLog(@"Create Utility");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)utilityId];
    OLMUtility *utility = [[OLMUtility alloc] init];
    [_utilities setObject:utility forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freeUtility:(NSUInteger*)utilityId)
{
    NSLog(@"Free Utility");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)utilityId];
    OLMUtility *utility = [_utilities objectForKey:id];
    if (utility == nil) {
        return nil;
    }
    [_utilities removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(sha256:(NSUInteger*)utilityId
                                       input:(NSArray*)input)
{
    NSLog(@"sha256 Utility");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)utilityId];
    OLMUtility *utility = [_utilities objectForKey:id];
    if (utility == nil) {
        return nil;
    }
    NSMutableData *mutableInput = [[NSMutableData alloc] initWithCapacity: [input count]];
    for (NSNumber *number in input) {
        char byte = [number charValue];
        [mutableInput appendBytes: &byte length: 1];
    }
    return [utility sha256:[NSData dataWithData:mutableInput]];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(ed25519_verify:(NSUInteger*)utilityId
                                       key:(NSString*)key
                                       message:(NSString*)message
                                       signature:(NSString*)signature)
{
    NSLog(@"ed25519_verify Utility");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)utilityId];
    OLMUtility *utility = [_utilities objectForKey:id];
    if (utility == nil) {
        return nil;
    }
    NSError *error;
    NSData* messageData = [message dataUsingEncoding:NSUTF8StringEncoding];
    return [utility verifyEd25519Signature:signature key:key message:messageData error:&error] ? (@(true)) : (@(false));
}

// =============================================>
// Session
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(initializeSession:(NSUInteger*)sessionId)
{
    NSLog(@"Init Session");
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freeSession:(NSUInteger*)sessionId)
{
    NSLog(@"Free Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    [_olmSessions removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(pickleSession:(NSUInteger*)sessionId
                                       key:(NSString*)key)
{
    NSLog(@"Pickle Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [session serializeDataWithKey:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(unpickleSession:(NSUInteger*)sessionId
                                       key:(NSString*)key
                                       pickle:(NSString*)pickle)
{
    NSLog(@"Unpickle Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        session = [[OLMSession alloc] init];
    }
    @try {
        NSError *error;
        NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
        session = [session initWithSerializedData:pickle key:keyData error:&error];
        if (error) {
            NSLog(@"olmSession unpickle() failed: %@", [error localizedDescription]);
        } else {
            [_olmSessions setObject:session forKey:id];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"olmSession unpickle() failed: %@", exception.reason);
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createOutboundSession:(NSUInteger*)sessionId
                                       account:(NSUInteger*)accountId
                                       their_identity_key:(NSString*)their_identity_key
                                       their_one_time_key:(NSString*)their_one_time_key)
{
    NSLog(@"Create Outbound Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        session = [[OLMSession alloc] init];
    }
    NSString *id2 = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id2];
    if (account == nil) {
        NSLog(@"Create Outbound Session - ACCOUNT NOT FOUND");
        return nil;
    }
    @try {
        NSError *error;
        session = [session initOutboundSessionWithAccount:account theirIdentityKey:their_identity_key theirOneTimeKey:their_one_time_key error:&error];
        if (error) {
            NSLog(@"olmSession create_outbound() failed: %@", [error localizedDescription]);
        } else {
            [_olmSessions setObject:session forKey:id];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"olmSession create_outbound() failed: %@", exception.reason);
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createInboundSession:(NSUInteger*)sessionId
                                       account:(NSUInteger*)accountId
                                       one_time_key_message:(NSString*)one_time_key_message)
{
    NSLog(@"Create Inbound Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        session = [[OLMSession alloc] init];
    }
    NSString *id2 = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id2];
    if (account == nil) {
        return nil;
    }
    @try {
        NSError *error;
        session = [session initInboundSessionWithAccount:account oneTimeKeyMessage:one_time_key_message error:&error];
        if (error) {
            NSLog(@"olmSession create_inbound() failed: %@", [error localizedDescription]);
        } else {
            [_olmSessions setObject:session forKey:id];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"olmSession create_inbound() failed: %@", exception.reason);
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createInboundSessionFrom:(NSUInteger*)sessionId
                                       account:(NSUInteger*)accountId
                                       identity_key:(NSString*)identity_key
                                       one_time_key_message:(NSString*)one_time_key_message)
{
    NSLog(@"Create Inbound Session From");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        session = [[OLMSession alloc] init];
    }
    NSString *id2 = [NSString stringWithFormat: @"%lu", (long)accountId];
    OLMAccount *account = [_accounts objectForKey:id2];
    if (account == nil) {
        return nil;
    }
    @try {
        NSError *error;
        session = [session initInboundSessionWithAccount:account theirIdentityKey:identity_key oneTimeKeyMessage:one_time_key_message error:&error];
        if (error) {
            NSLog(@"olmSession create_inbound_from() failed: %@", [error localizedDescription]);
        } else {
            [_olmSessions setObject:session forKey:id];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"olmSession create_inbound_from() failed: %@", exception.reason);
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getSessionId:(NSUInteger*)sessionId)
{
    NSLog(@"Get Session Id");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    return session.sessionIdentifier;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(sessionHasReceivedMessage:(NSUInteger*)sessionId)
{
    NSLog(@"Session Has Received Message: not implemented");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(sessionMatchesInbound:(NSUInteger*)sessionId
                                       one_time_key_message:(NSString*)one_time_key_message)
{
    NSLog(@"Session Matches Inbound");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    return [session matchesInboundSession:one_time_key_message] ? (@(true)) : (@(false));
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(sessionMatchesInboundFrom:(NSUInteger*)sessionId
                                       identity_key:(NSString*)identity_key
                                       one_time_key_message:(NSString*)one_time_key_message)
{
    NSLog(@"Session Matches Inbound From");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    return [session matchesInboundSessionFrom:identity_key oneTimeKeyMessage:one_time_key_message] ? (@(true)) : (@(false));
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(encryptSession:(NSUInteger*)sessionId
                                       plaintext:(NSString*)plaintext)
{
    NSLog(@"Encrypt Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (session == nil) {
        return map;
    }
    @try {
        NSError *error;
        OLMMessage *encryptedOlmMessage = [session encryptMessage:plaintext error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [map setObject:[encryptedOlmMessage ciphertext] forKey:@"body"];
            [map setObject:@([encryptedOlmMessage type]) forKey:@"type"];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(decryptSession:(NSUInteger*)sessionId
                                       message_type:(NSUInteger*)message_type
                                       message:(NSString*)message)
{
    NSLog(@"Decrypt Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return map;
    }
    @try {
        NSError *error;
        OLMMessage *msg = [[OLMMessage alloc] init];
        msg = [msg initWithCiphertext:message type:(long)message_type];
        NSString *decryptedMessage = [session decryptMessage:msg error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [map setObject:decryptedMessage forKey:@"result"];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(describeSession:(NSUInteger*)sessionId)
{
    NSLog(@"Describe Session: not implemented");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMSession *session = [_olmSessions objectForKey:id];
    if (session == nil) {
        return nil;
    }
    return nil;
}

// =============================================>
// SAS
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createSAS:(NSUInteger*)sasId)
{
    NSLog(@"Create SAS");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [[OLMSAS alloc] init];
    [_olmSASs setObject:olmSAS forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freeSAS:(NSUInteger*)sasId)
{
    NSLog(@"Free SAS");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [_olmSASs objectForKey:id];
    if (olmSAS == nil) {
        return nil;
    }
    [_olmSASs removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getSASPubKey:(NSUInteger*)sasId)
{
    NSLog(@"Get SAS Public Key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [_olmSASs objectForKey:id];
    if (olmSAS == nil) {
        return nil;
    }
    return [olmSAS publicKey];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(setTheirSASKey:(NSUInteger*)sasId
                                       their_key:(NSString*)their_key)
{
    NSLog(@"Set Their SAS Key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [_olmSASs objectForKey:id];
    if (olmSAS == nil) {
        return nil;
    }
    return [olmSAS setTheirPublicKey:their_key];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(generateSASBytes:(NSUInteger*)sasId
                                       info:(NSString*)info
                                       length:(NSUInteger*)length)
{
    NSLog(@"Generate SAS Bytes");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [_olmSASs objectForKey:id];
    if (olmSAS == nil) {
        return nil;
    }
    return [olmSAS generateBytes:info length:(NSUInteger)length];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(calculateSASMac:(NSUInteger*)sasId
                                       input:(NSString*)input
                                       info:(NSString*)info)
{
    NSLog(@"Calculate SAS Mac");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [_olmSASs objectForKey:id];
    if (olmSAS == nil) {
        return nil;
    }
    return [olmSAS calculateMac:input info:info error:NULL];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(calculateSASMacLongKdf:(NSUInteger*)sasId
                                       input:(NSString*)input
                                       info:(NSString*)info)
{
    NSLog(@"Calculate SAS Mac");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sasId];
    OLMSAS *olmSAS = [_olmSASs objectForKey:id];
    if (olmSAS == nil) {
        return nil;
    }
    return [olmSAS calculateMacLongKdf:input info:info error:NULL];
}

// =============================================>
// PKSigning
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createPkSigning:(NSUInteger*)pkSigningId)
{
    NSLog(@"Create PK Signing");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkSigningId];
    OLMPkSigning *olmPkSigning = [[OLMPkSigning alloc] init];
    [_olmPkSignings setObject:olmPkSigning forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freePkSigning:(NSUInteger*)pkSigningId)
{
    NSLog(@"Free PK Signing");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkSigningId];
    OLMPkSigning *olmPkSigning = [_olmPkSignings objectForKey:id];
    if (olmPkSigning == nil) {
        return nil;
    }
    [_olmPkSignings removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(initializePkSigningWithSeed:(NSUInteger*)pkSigningId
                                       seed:(NSArray*)seed)
{
    NSLog(@"Init PK Signing With Seed");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkSigningId];
    OLMPkSigning *olmPkSigning = [_olmPkSignings objectForKey:id];
    if (olmPkSigning == nil) {
        return nil;
    }
    NSMutableData *mutableSeed = [[NSMutableData alloc] initWithCapacity: [seed count]];
    for (NSNumber *number in seed) {
        char byte = [number charValue];
        [mutableSeed appendBytes: &byte length: 1];
    }
    return [olmPkSigning doInitWithSeed:[NSData dataWithData:mutableSeed] error:NULL];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(generatePkSigningSeed:(NSUInteger*)pkSigningId)
{
    NSLog(@"Generate PK Signing With Seed");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkSigningId];
    OLMPkSigning *olmPkSigning = [_olmPkSignings objectForKey:id];
    if (olmPkSigning == nil) {
        return nil;
    }
    NSData *seed = [OLMPkSigning generateSeed];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    const char *bytes = [seed bytes];
    for (int i = 0; i < [seed length]; i++) {
        NSNumber* charNum = [NSNumber numberWithInt:(unsigned char)bytes[i]];
        [arr addObject:charNum];
    }
    return arr;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(signPkSigning:(NSUInteger*)pkSigningId
                                       message:(NSString*)message)
{
    NSLog(@"Sign PK Signing");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkSigningId];
    OLMPkSigning *olmPkSigning = [_olmPkSignings objectForKey:id];
    if (olmPkSigning == nil) {
        return nil;
    }
    return [olmPkSigning sign:message error:NULL];
}

// =============================================>
// PKEncryption
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createPkEncryption:(NSUInteger*)pkEncryptionId)
{
    NSLog(@"Create PK Encryption");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkEncryptionId];
    OLMPkEncryption *olmPkEncryption = [[OLMPkEncryption alloc] init];
    [_olmPkEncryptions setObject:olmPkEncryption forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freePkEncryption:(NSUInteger*)pkEncryptionId)
{
    NSLog(@"Free PK Encryption");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkEncryptionId];
    OLMPkEncryption *olmPkEncryption = [_olmPkEncryptions objectForKey:id];
    if (olmPkEncryption == nil) {
        return nil;
    }
    [_olmPkEncryptions removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(setPkEncryptionRecipientKey:(NSUInteger*)pkEncryptionId
                                       key:(NSString*)key)
{
    NSLog(@"Set PK Encryption Recipient Key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkEncryptionId];
    OLMPkEncryption *olmPkEncryption = [_olmPkEncryptions objectForKey:id];
    if (olmPkEncryption == nil) {
        return nil;
    }
    [olmPkEncryption setRecipientKey:key];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(encryptPkEncryption:(NSUInteger*)pkEncryptionId
                                       plaintext:(NSString*)plaintext)
{
    NSLog(@"Encrypt PK Encryption");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkEncryptionId];
    OLMPkEncryption *olmPkEncryption = [_olmPkEncryptions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (olmPkEncryption == nil) {
        return map;
    }
    @try {
        NSError *error;
        OLMPkMessage *encryptedOlmPkMessage = [olmPkEncryption encryptMessage:plaintext error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [map setObject:[encryptedOlmPkMessage ciphertext] forKey:@"ciphertext"];
            [map setObject:[encryptedOlmPkMessage mac] forKey:@"mac"];
            [map setObject:[encryptedOlmPkMessage ephemeralKey] forKey:@"ephemeral"];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

// =============================================>
// PKDecryption
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createPkDecryption:(NSUInteger*)pkDecryptionId)
{
    NSLog(@"Create PK Decryption");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [[OLMPkDecryption alloc] init];
    [_olmPkDecryptions setObject:olmPkDecryption forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freePkDecryption:(NSUInteger*)pkDecryptionId)
{
    NSLog(@"Free PK Decryption");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    if (olmPkDecryption == nil) {
        return nil;
    }
    [_olmPkDecryptions removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(initializePkDecryptionWithPrivateKey:(NSUInteger*)pkDecryptionId
                                       key:(NSArray*)key)
{
    NSLog(@"init PK Decryption with private key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    if (olmPkDecryption == nil) {
        return nil;
    }
    NSMutableData *mutableKey = [[NSMutableData alloc] initWithCapacity: [key count]];
    for (NSNumber *number in key) {
        char byte = [number charValue];
        [mutableKey appendBytes: &byte length: 1];
    }
    NSError *error;
    return [olmPkDecryption setPrivateKey:[NSData dataWithData:mutableKey] error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(generatePkDecryptionKey:(NSUInteger*)pkDecryptionId)
{
    NSLog(@"generate PK decryption key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    if (olmPkDecryption == nil) {
        return nil;
    }
    NSError *error;
    return [olmPkDecryption generateKey:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getPkDecryptionPrivateKey:(NSUInteger*)pkDecryptionId)
{
    NSLog(@"get PK Decryption private key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    if (olmPkDecryption == nil) {
        return nil;
    }
    NSData *privateKey = [olmPkDecryption privateKey];
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    const char *bytes = [privateKey bytes];
    for (int i = 0; i < [privateKey length]; i++) {
        NSNumber* charNum = [NSNumber numberWithInt:(unsigned char)bytes[i]];
        [arr addObject:charNum];
    }
    return arr;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(picklePkDecryption:(NSUInteger*)pkDecryptionId
                                       key:(NSString*)key)
{
    NSLog(@"pickle pk decryption key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    if (olmPkDecryption == nil) {
        return nil;
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [olmPkDecryption serializeDataWithKey:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(unpicklePkDecryption:(NSUInteger*)pkDecryptionId
                                       key:(NSString*)key
                                       pickle:(NSString*)pickle)
{
    NSLog(@"unpickle pk decryption key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    if (olmPkDecryption == nil) {
        return nil;
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [olmPkDecryption initWithSerializedData:pickle key:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(decryptPkDecryption:(NSUInteger*)pkDecryptionId
                                       ephemeral_key:(NSString*)ephemeral_key
                                       mac:(NSString*)mac
                                       ciphertext:(NSString*)ciphertext)
{
    NSLog(@"decrypt PK Decryption");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)pkDecryptionId];
    OLMPkDecryption *olmPkDecryption = [_olmPkDecryptions objectForKey:id];
    NSMutableDictionary *map = [[NSMutableDictionary alloc] init];
    if (olmPkDecryption == nil) {
        return map;
    }
    OLMPkMessage *msg = [[OLMPkMessage alloc] init];
    msg = [msg initWithCiphertext:ciphertext mac:mac ephemeralKey:ephemeral_key];
    @try {
        NSError *error;
        NSString *decryptedMessage = [olmPkDecryption decryptMessage:msg error:&error];
        if (error) {
            [map setObject:@(true) forKey:@"error"];
            [map setObject:[error localizedDescription] forKey:@"errorMessage"];
        } else {
            [map setObject:decryptedMessage forKey:@"result"];
        }
    }
    @catch (NSException *exception) {
        [map setObject:@(true) forKey:@"error"];
        [map setObject:exception.reason forKey:@"errorMessage"];
    }
    return map;
}

// =============================================>
// Outbound Group Session
// =============================================>

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(initializeOutboundGroupSession:(NSUInteger*)sessionId)
{
    NSLog(@"Init Outbound Group Session");
    return nil;
}


RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(createOutboundGroupSession:(NSUInteger*)sessionId)
{
    NSLog(@"create OutboundGroupSession");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [[OLMOutboundGroupSession alloc] init].initOutboundGroupSession;
    [_olmOutboundGroupSessions setObject:olmOutboundGroupSession forKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(freeOutboundGroupSession:(NSUInteger*)sessionId)
{
    NSLog(@"Free Outbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        return nil;
    }
    [_olmOutboundGroupSessions removeObjectForKey:id];
    return nil;
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(pickleOutboundGroupSession:(NSUInteger*)sessionId
                                       key:(NSString *)key)
{
    NSLog(@"pickle outbound group session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        return nil;
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [olmOutboundGroupSession serializeDataWithKey:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(unpickleOutboundGroupSession:(NSUInteger*)sessionId
                                       key:(NSString *)key
                                       pickle:(NSString *)pickle)
{
    NSLog(@"unpickle outbound group session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        olmOutboundGroupSession = [[OLMOutboundGroupSession alloc] init];
        [_olmOutboundGroupSessions setObject:olmOutboundGroupSession forKey:id];
    }
    NSError *error;
    NSData* keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    return [olmOutboundGroupSession initWithSerializedData:pickle key:keyData error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(encryptOutboundGroupSession:(NSUInteger*)sessionId
                                       plaintext:(NSString*)plaintext)
{
    NSLog(@"encrypt Outbound Group Session");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        return nil;
    }
    NSError *error;
    return [olmOutboundGroupSession encryptMessage:plaintext error:&error];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getOutboundGroupSessionSessionId:(NSUInteger*)sessionId)
{
    NSLog(@"get Outbound Group Session session ID");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        return nil;
    }
    return [olmOutboundGroupSession sessionIdentifier];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getOutboundGroupSessionSessionKey:(NSUInteger*)sessionId)
{
    NSLog(@"get Outbound Group Session session key");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        return nil;
    }
    return [olmOutboundGroupSession sessionKey];
}

RCT_EXPORT_BLOCKING_SYNCHRONOUS_METHOD(getOutboundGroupSessionMessageIndex:(NSUInteger*)sessionId)
{
    NSLog(@"get Outbound Group Session message index");
    NSString *id = [NSString stringWithFormat: @"%lu", (long)sessionId];
    OLMOutboundGroupSession *olmOutboundGroupSession = [_olmOutboundGroupSessions objectForKey:id];
    if (olmOutboundGroupSession == nil) {
        return nil;
    }
    return @((long) [olmOutboundGroupSession messageIndex]);
}

@end
