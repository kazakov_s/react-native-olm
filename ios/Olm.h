#import <React/RCTBridgeModule.h>
#import <OLMKit/OLMKit.h>
#import <React/RCTLog.h>

@interface Olm : NSObject <RCTBridgeModule>

@property (nonatomic) NSMutableDictionary *accounts;
@property (nonatomic) NSMutableDictionary *olmInboundGroupSessions;
@property (nonatomic) NSMutableDictionary *olmOutboundGroupSessions;
@property (nonatomic) NSMutableDictionary *olmSessions;
@property (nonatomic) NSMutableDictionary *olmPkSignings;
@property (nonatomic) NSMutableDictionary *olmPkEncryptions;
@property (nonatomic) NSMutableDictionary *olmPkDecryptions;
@property (nonatomic) NSMutableDictionary *olmSASs;
@property (nonatomic) NSMutableDictionary *utilities;
@property (nonatomic) OLMKit *olmKit;

@end
