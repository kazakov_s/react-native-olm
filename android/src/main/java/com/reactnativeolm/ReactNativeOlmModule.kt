package com.reactnativeolm

import android.util.Log
import com.facebook.react.bridge.*
import org.json.JSONObject
import org.matrix.olm.*

class ReactNativeOlmModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
  override fun getName(): String {
    return "ReactNativeOlm"
  }

  // =============================================>
  // Account
  // =============================================>
  private val accounts: MutableMap<Int, OlmAccount?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun initializeAccount(id: Int) {
    this.accounts.put(id, null)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createAccount(id: Int) {
    this.accounts.put(id, OlmAccount())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freeAccount(id: Int) {
    val account = this.accounts.get(id)
    if (account == null) {
      return
    }
    account!!.releaseAccount()
    this.accounts.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun identity_keys(id: Int): String? {
    val account = this.accounts.get(id)
    if (account == null) {
      return null
    }
    val identityKeys = account!!.identityKeys()
    if (identityKeys === null) {
      return JSONObject().toString()
    }
    return JSONObject(account!!.identityKeys() as Map<*, *>).toString()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun signAccount(id: Int, msg: String): String? {
    val account = this.accounts.get(id)
    if (account == null) {
      return null
    }
    return account!!.signMessage(msg)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun one_time_keys(id: Int): String? {
    val account = this.accounts.get(id)
    if (account == null) {
      return null
    }
    val oneTimeKeys = account!!.oneTimeKeys()

    if (oneTimeKeys === null) {
      return null
    }

    return JSONObject(account!!.oneTimeKeys() as Map<*, *>).toString()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun mark_keys_as_published(id: Int) {
    val account = this.accounts.get(id)
    if (account == null) {
      return
    }
    account!!.markOneTimeKeysAsPublished()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun max_number_of_one_time_keys(id: Int): Int? {
    val account = this.accounts.get(id)
    if (account == null) {
      return null
    }
    return account!!.maxOneTimeKeys()!!.toInt()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun generate_one_time_keys(id: Int, numberOfKeys: Int) {
    val account = this.accounts.get(id)
    if (account == null) {
      return
    }
    try {
      account!!.generateOneTimeKeys(numberOfKeys)
    } catch (e: Exception) {
      Log.e("OlmAccount", "## generate_one_time_keys() failed " + e.toString())
    }
  }

  // TODO: We have to change olm/android's removeOneTimeKeys method
  //  to only receive a sessionId rather than a Session instance
  @ReactMethod(isBlockingSynchronousMethod = true)
  fun remove_one_time_keys(id: Int, sessionId: Int) {
    val account = this.accounts.get(id)
    if (account == null) {
      return
    }
    val session = this.olmSessions.get(sessionId)
    if (session == null) {
      return
    }
    try {
      account!!.removeOneTimeKeys(session)
    } catch (e: Exception) {
      Log.e("OlmAccount", "## remove_one_time_keys() failed " + e.toString())
    }
  }

  // TODO: this is not implemented in olm/android.
  //  Maybe it's okay to leave it out?
  @ReactMethod(isBlockingSynchronousMethod = true)
  fun generate_fallback_key(id: Int) {
    val account = this.accounts.get(id)
    if (account == null) {
      return
    }
    try {
      account!!.generateFallbackKey()
    } catch (e: Exception) {
      Log.e("OlmAccount", "## generate_fallback_key() failed " + e.toString())
    }
  }

  // TODO: this is not implemented in olm/android.
  //  Maybe it's okay to leave it out?
  @ReactMethod(isBlockingSynchronousMethod = true)
  fun fallback_key(id: Int): String? {
    val account = this.accounts.get(id)
    if (account == null) {
      return null
    }
    val fallbackKey = account!!.fallbackKey()

    if (fallbackKey == null) {
      return JSONObject().toString()
    }

    return JSONObject(account!!.oneTimeKeys() as Map<*, *>).toString()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun pickleAccount(id: Int, key: String?): String? {
    val account = this.accounts.get(id)
    if (account == null) {
      return null
    }
    val buffer = StringBuffer()
    val pickledAccount = String(account!!.pickle(key?.toByteArray(Charsets.UTF_8), buffer), Charsets.UTF_8)
    return pickledAccount
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun unpickleAccount(id: Int, key: String, pickle: String) {
    var account = this.accounts.get(id)
    if (account == null) {
      account = OlmAccount()
      this.accounts.put(id, account)
    }
    try {
      account!!.unpickle(pickle.toByteArray(Charsets.UTF_8), key.toByteArray(Charsets.UTF_8))
    } catch (e: Exception) {
      Log.e("OlmAccount", "## unpickle() failed " + e.toString())
    }
  }


  // =============================================>
  // OlmInboundGroupSession
  // =============================================>
  private val olmInboundGroupSessions: MutableMap<Int, OlmInboundGroupSession?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun initializeInboundGroupSession(id: Int) {
    this.olmInboundGroupSessions.put(id, null)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createInboundGroupSession(id: Int, session_key: String) {
    this.olmInboundGroupSessions.put(id, OlmInboundGroupSession(session_key))
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freeInboundGroupSession(id: Int) {
    val olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      return
    }
    olmInboundGroupSession!!.releaseSession()
    this.olmInboundGroupSessions.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun pickleInboundGroupSession(id: Int, key: String): String? {
    val olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      return null
    }
    val buffer = StringBuffer()
    val pickledSession = String(olmInboundGroupSession!!.pickle(key?.toByteArray(Charsets.UTF_8), buffer), Charsets.UTF_8)
    return pickledSession
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun unpickleInboundGroupSession(id: Int, key: String, pickle: String) {
    var olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      val olmOutboundGroupSession = OlmOutboundGroupSession()
      olmOutboundGroupSession.sessionKey()
      olmInboundGroupSession = OlmInboundGroupSession(olmOutboundGroupSession.sessionKey())
      olmOutboundGroupSession.releaseSession()
      this.olmInboundGroupSessions.put(id, olmInboundGroupSession)
    }
    try {
      olmInboundGroupSession!!.unpickle(pickle.toByteArray(Charsets.UTF_8), key.toByteArray(Charsets.UTF_8))
    } catch (e: Exception) {
      Log.e("olmInboundGroupSession", "## unpickle() failed " + e.toString())
    }
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun importInboundGroupSession(id: Int, session_key: String): WritableMap {
    val map = WritableNativeMap()
    try {
      val olmInboundGroupSession = OlmInboundGroupSession.importSession(session_key)
      this.olmInboundGroupSessions.put(id, olmInboundGroupSession)
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun decryptInboundGroupSession(id: Int, message: String): WritableMap {
    val map = WritableNativeMap()
    val olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      return map
    }
    try {
      val olmMessage = olmInboundGroupSession!!.decryptMessage(message)
      if (olmMessage !== null) {
        map.putString("plaintext", olmMessage.mDecryptedMessage)
        map.putInt("message_index", olmMessage.mIndex.toInt())
      }
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getInboundGroupSessionSessionId(id: Int): WritableMap {
    val map = WritableNativeMap()
    val olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      return map
    }
    try {
      val result = olmInboundGroupSession!!.sessionIdentifier()
      map.putString("result", result)
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getInboundGroupSessionFirstKnownIndex(id: Int): WritableMap {
    val map = WritableNativeMap()
    val olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      return map
    }
    try {
      val result = olmInboundGroupSession!!.firstKnownIndex!!.toInt()
      if (result !== null) {
        map.putInt("result", result)
      }
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun exportInboundGroupSession(id: Int, message_index: Int): WritableMap {
    val map = WritableNativeMap()
    val olmInboundGroupSession = this.olmInboundGroupSessions.get(id)
    if (olmInboundGroupSession == null) {
      return map
    }
    try {
      val result = olmInboundGroupSession!!.export(message_index.toLong())
      if (result !== null) {
        map.putString("result", result)
      }
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  // =============================================>
  // OlmManager
  // =============================================>
  var olmManager: OlmManager? = null

  @ReactMethod
  fun initializeOlm(promise: Promise) {
    this.olmManager = OlmManager()
    promise.resolve(OlmPkDecryption.privateKeyLength())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getOlmLibVersion(): String? {
    return this.olmManager!!.olmLibVersion
  }

  // =============================================>
  // OlmOutboundGroupSession
  // =============================================>
  private val olmOutboundGroupSessions: MutableMap<Int, OlmOutboundGroupSession?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun initializeOutboundGroupSession(id: Int) {
    this.olmOutboundGroupSessions.put(id, null)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createOutboundGroupSession(id: Int) {
    this.olmOutboundGroupSessions.put(id, OlmOutboundGroupSession())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freeOutboundGroupSession(id: Int) {
    val olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      return
    }
    olmOutboundGroupSession!!.releaseSession()
    this.olmOutboundGroupSessions.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun pickleOutboundGroupSession(id: Int, key: String): String? {
    val olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      return null
    }
    val buffer = StringBuffer()
    val pickledSession = String(olmOutboundGroupSession!!.pickle(key?.toByteArray(Charsets.UTF_8), buffer), Charsets.UTF_8)
    return pickledSession
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun unpickleOutboundGroupSession(id: Int, key: String, pickle: String) {
    var olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      olmOutboundGroupSession = OlmOutboundGroupSession()
      this.olmOutboundGroupSessions.put(id, olmOutboundGroupSession)
    }
    try {
      olmOutboundGroupSession!!.unpickle(pickle.toByteArray(Charsets.UTF_8), key.toByteArray(Charsets.UTF_8))
    } catch (e: Exception) {
      Log.e("olmOutboundGroupSession", "## unpickle() failed " + e.toString())
    }
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun encryptOutboundGroupSession(id: Int, plaintext: String): String? {
    val olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      return null
    }
    return olmOutboundGroupSession!!.encryptMessage(plaintext)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getOutboundGroupSessionSessionId(id: Int): String? {
    val olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      return null
    }
    return olmOutboundGroupSession!!.sessionIdentifier()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getOutboundGroupSessionSessionKey(id: Int): String? {
    val olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      return null
    }
    return olmOutboundGroupSession!!.sessionKey()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getOutboundGroupSessionMessageIndex(id: Int): Int? {
    val olmOutboundGroupSession = this.olmOutboundGroupSessions.get(id)
    if (olmOutboundGroupSession == null) {
      return null
    }
    return olmOutboundGroupSession!!.messageIndex()
  }

  // =============================================>
  // OlmPkDecryption
  // =============================================>
  private val olmPkDecryptions: MutableMap<Int, OlmPkDecryption?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createPkDecryption(id: Int) {
    this.olmPkDecryptions.put(id, OlmPkDecryption())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freePkDecryption(id: Int) {
    val olmPkDecryption = this.olmPkDecryptions.get(id)
    if (olmPkDecryption == null) {
      return
    }
    olmPkDecryption!!.releaseDecryption()
    this.olmPkDecryptions.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun initializePkDecryptionWithPrivateKey(id: Int, key: ReadableArray): String? {
    val olmPkDecryption = this.olmPkDecryptions.get(id)
    if (olmPkDecryption == null) {
      return null
    }
    return olmPkDecryption!!.setPrivateKey(ConversionUtils.convertArrayToByteArray(key))
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun generatePkDecryptionKey(id: Int): String? {
    val olmPkDecryption = this.olmPkDecryptions.get(id)
    if (olmPkDecryption == null) {
      return null
    }
    return olmPkDecryption!!.generateKey()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getPkDecryptionPrivateKey(id: Int): WritableArray {
    val arr = WritableNativeArray()
    val olmPkDecryption = this.olmPkDecryptions.get(id)
    if (olmPkDecryption == null) {
      return arr
    }
    val privateKeyArray = olmPkDecryption!!.privateKey()
    privateKeyArray!!.forEach { byte ->
      arr.pushInt(byte.toInt())
    }

    return arr
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun decryptPkDecryption(id: Int, ephemeral_key: String?, mac: String?, ciphertext: String?): WritableMap? {
    val map = WritableNativeMap()
    val olmPkDecryption = this.olmPkDecryptions.get(id)
    if (olmPkDecryption == null) {
      return map
    }

    try {
      val olmPkMessage = OlmPkMessage()
      olmPkMessage.mCipherText = ciphertext
      olmPkMessage.mMac = mac
      olmPkMessage.mEphemeralKey = ephemeral_key

      val result = olmPkDecryption!!.decrypt(olmPkMessage)
      map.putString("result", result)
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  // =============================================>
  // OlmPkEncryption
  // =============================================>
  private val olmPkEncryptions: MutableMap<Int, OlmPkEncryption?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createPkEncryption(id: Int) {
    this.olmPkEncryptions.put(id, OlmPkEncryption())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freePkEncryption(id: Int) {
    val olmPkEncryption = this.olmPkEncryptions.get(id)
    if (olmPkEncryption == null) {
      return
    }
    olmPkEncryption!!.releaseEncryption()
    this.olmPkEncryptions.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun setPkEncryptionRecipientKey(id: Int, key: String) {
    val olmPkEncryption = this.olmPkEncryptions.get(id)
    if (olmPkEncryption == null) {
      return
    }
    olmPkEncryption!!.setRecipientKey(key)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun encryptPkEncryption(id: Int, plaintext: String): WritableMap {
    val map = WritableNativeMap()
    val olmPkEncryption = this.olmPkEncryptions.get(id)
    if (olmPkEncryption == null) {
      return map
    }
    val olmPkMessage = olmPkEncryption!!.encrypt(plaintext)
    if (olmPkMessage != null) {
      map.putString("ciphertext", olmPkMessage.mCipherText)
      map.putString("mac", olmPkMessage.mMac)
      map.putString("ephemeral", olmPkMessage.mEphemeralKey)
    }

    return map
  }

  // =============================================>
  // OlmPkSigning
  // =============================================>
  private val olmPkSignings: MutableMap<Int, OlmPkSigning?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createPkSigning(id: Int) {
    this.olmPkSignings.put(id, OlmPkSigning())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freePkSigning(id: Int) {
    val olmPkSigning = this.olmPkSignings.get(id)
    if (olmPkSigning == null) {
      return
    }
    olmPkSigning!!.releaseSigning()
    this.olmPkSignings.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun initializePkSigningWithSeed(id: Int, seed: ReadableArray): String? {
    val olmPkSigning = this.olmPkSignings.get(id)
    if (olmPkSigning == null) {
      return null
    }
    return olmPkSigning!!.initWithSeed(ConversionUtils.convertArrayToByteArray(seed))
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun generatePkSigningSeed(id: Int): WritableArray {
    val arr = WritableNativeArray()
    val olmPkSigning = this.olmPkSignings.get(id)
    if (olmPkSigning == null) {
      return arr
    }
    return ConversionUtils.convertByteArrayToArray(OlmPkSigning.generateSeed())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun signPkSigning(id: Int, message: String): String? {
    val olmPkSigning = this.olmPkSignings.get(id)
    if (olmPkSigning == null) {
      return null
    }
    return olmPkSigning!!.sign(message)
  }

  // =============================================>
  // OlmSAS
  // =============================================>
  private val olmSASs: MutableMap<Int, OlmSAS?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createSAS(id: Int) {
    this.olmSASs.put(id, OlmSAS())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freeSAS(id: Int) {
    val olmSAS = this.olmSASs.get(id)
    if (olmSAS == null) {
      return
    }
    olmSAS!!.releaseSas()
    this.olmSASs.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getSASPubKey(id: Int): String? {
    val olmSAS = this.olmSASs.get(id)
    if (olmSAS == null) {
      return null
    }
    return olmSAS!!.publicKey
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun setTheirSASKey(id: Int, their_key: String) {
    val olmSAS = this.olmSASs.get(id)
    if (olmSAS == null) {
      return
    }
    olmSAS!!.setTheirPublicKey(their_key)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun generateSASBytes(id: Int, info: String, length: Int): WritableArray {
    val arr = WritableNativeArray()
    val olmSAS = this.olmSASs.get(id)
    if (olmSAS == null) {
      return arr
    }
    return ConversionUtils.convertByteArrayToArray(olmSAS!!.generateShortCode(info, length)!!)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun calculateSASMac(id: Int, input: String, info: String): String? {
    val olmSAS = this.olmSASs.get(id)
    if (olmSAS == null) {
      return null
    }
    return olmSAS!!.calculateMac(input, info)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun calculateSASMacLongKdf(id: Int, input: String, info: String): String? {
    val olmSAS = this.olmSASs.get(id)
    if (olmSAS == null) {
      return null
    }
    return olmSAS!!.calculateMacLongKdf(input, info)
  }

  // =============================================>
  // OlmSession
  // =============================================>
  private val olmSessions: MutableMap<Int, OlmSession?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun initializeSession(id: Int) {
    this.olmSessions.put(id, OlmSession())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freeSession(id: Int) {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return
    }
    olmSession!!.releaseSession()
    this.olmSessions.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun pickleSession(id: Int, key: String): String? {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return null
    }
    val buffer = StringBuffer()
    val pickledSession = String(olmSession!!.pickle(key?.toByteArray(Charsets.UTF_8), buffer), Charsets.UTF_8)
    return pickledSession
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun unpickleSession(id: Int, key: String, pickle: String) {
    var olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      olmSession = OlmSession()
      this.olmSessions.put(id, olmSession)
    }
    try {
      olmSession!!.unpickle(pickle.toByteArray(Charsets.UTF_8), key.toByteArray(Charsets.UTF_8))
    } catch (e: Exception) {
      Log.e("olmSession", "## unpickle() failed " + e.toString())
    }
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createOutboundSession(
    id: Int,
    accountId: Int,
    their_identity_key: String,
    their_one_time_key: String
  ) {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return
    }
    val account = this.accounts.get(accountId)
    if (account == null) {
      return
    }
    try {
      olmSession!!.initOutboundSession(account, their_identity_key, their_one_time_key)
    } catch (e: Exception) {
      Log.e("olmSession", "## initOutboundSession() failed " + e.toString())
    }
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createInboundSession(id: Int, accountId: Int, one_time_key_message: String) {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return
    }
    val account = this.accounts.get(accountId)
    if (account == null) {
      return
    }
    try {
      olmSession!!.initInboundSession(account, one_time_key_message)
    } catch (e: Exception) {
      Log.e("olmSession", "## initInboundSession() failed " + e.toString())
    }
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createInboundSessionFrom(
    id: Int,
    accountId: Int,
    identity_key: String,
    one_time_key_message: String
  ) {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return
    }
    val account = this.accounts.get(accountId)
    if (account == null) {
      return
    }
    try {
      olmSession!!.initInboundSessionFrom(account, identity_key, one_time_key_message)
    } catch (e: Exception) {
      Log.e("olmSession", "## initInboundSessionFrom() failed " + e.toString())
    }
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun getSessionId(id: Int): String? {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return null
    }
    return olmSession!!.sessionIdentifier()
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun sessionHasReceivedMessage(id: Int): Boolean? {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return null
    }
    // TODO
    return null
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun sessionMatchesInbound(id: Int, one_time_key_message: String): Boolean? {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return null
    }
    return olmSession!!.matchesInboundSession(one_time_key_message)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun sessionMatchesInboundFrom(id: Int, identity_key: String, one_time_key_message: String): Boolean? {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return null
    }
    return olmSession!!.matchesInboundSessionFrom(identity_key, one_time_key_message)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun encryptSession(id: Int, plaintext: String): WritableMap? {
    val map = WritableNativeMap()
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return map
    }
    val olmMessage: OlmMessage?
    try {
      olmMessage = olmSession!!.encryptMessage(plaintext)
      if (olmMessage != null) {
        map.putString("body", olmMessage.mCipherText)
        map.putInt("type", olmMessage.mType.toInt())
      }
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun decryptSession(id: Int, message_type: Int, message: String): WritableMap {
    val map = WritableNativeMap()
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return map
    }
    val olmMessage = OlmMessage()
    olmMessage.mCipherText = message
    olmMessage.mType = message_type.toLong()
    try {
      val result = olmSession!!.decryptMessage(olmMessage)
      map.putString("result", result)
    } catch (e: Exception) {
      map.putBoolean("error", true)
      map.putString("errorMessage", e.toString())
    }

    return map
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun describeSession(id: Int): String? {
    val olmSession = this.olmSessions.get(id)
    if (olmSession == null) {
      return null
    }
    return "NOT IMPLEMENTED"
  }

  // =============================================>
  // OlmUtility
  // =============================================>
  private val olmUtilities: MutableMap<Int, OlmUtility?> = mutableMapOf()

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun createUtility(id: Int) {
    this.olmUtilities.put(id, OlmUtility())
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun freeUtility(id: Int) {
    val olmUtility = this.olmUtilities.get(id)
    if (olmUtility == null) {
      return
    }
    olmUtility!!.releaseUtility()
    this.olmUtilities.remove(id)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun sha256(id: Int, message: String): String? {
    val olmUtility = this.olmUtilities.get(id)
    if (olmUtility == null) {
      return null
    }
    return olmUtility!!.sha256(message)
  }

  @ReactMethod(isBlockingSynchronousMethod = true)
  fun ed25519_verify(id: Int, key: String, message: String, signature: String): Boolean {
    val olmUtility = this.olmUtilities.get(id)
    if (olmUtility == null) {
      return false
    }
    try {
      olmUtility!!.verifyEd25519Signature(signature, key, message)
    } catch (e: Exception) {
      return false
    }

    return true
  }
}
